<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'name', 'name_formatted', 'url', 'bx_code', 'email', 'phone', 'whatsapp', 'status'
    ];

}
