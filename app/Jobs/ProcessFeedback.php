<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Connectors\BitrixConnector;
use App\Lead;

class ProcessFeedback implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $lead;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Lead $lead)
    {
        $this->lead = $lead;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(BitrixConnector $connector)
    {
        $visits = $this->lead->visits;

		$result = $connector->addLead([
            'title' =>  $this->lead->name,
            'name'  =>  $this->lead->name,
            'phone' =>  $this->lead->phone,
			'visits' => $this->lead->visits,
            'direction' =>  56,
			'comment'	=>	$this->lead->text,
            'description'   =>  $visits,
            'city'  =>  $this->lead->bx_code,
            'source'    =>  'WEB'
        ]);
    }
}
