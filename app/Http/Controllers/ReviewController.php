<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use File;
use Illuminate\Support\Str;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = Review::All();

        $data = [
            'title' => 'Отзывы',
            'reviews' => $reviews,
        ];

        return view('backend.review.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'Новый отзыв',
        ];

        return view('backend.review.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');
         
        if($request->file()):
            $dir = public_path('uploads/reviews/');
            $files = $request->file();
            foreach($files as $tag => $array){
                foreach($array as $file){
                    $name = Str::random(5) .'.' . $file->getClientOriginalExtension() ?: 'jpg';
                    $img = Image::make($file);
                    $img->save($dir . $name);
                    $input[$tag] = $name;
                }
            }
        endif;

        $review = Review::create($input);

        if ($review->save()) {    
            return redirect()->route('reviews.index')->with('status','Отзыв добавлен');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        $old = $review->toArray();

        $dir = public_path('uploads/reviews/');

        if($review->avatar){

            if(file_exists($dir . $review->avatar)):

                $avatar = [
                    "name" => $review->avatar,
                    "size" => filesize($dir . $review->avatar),
                    "file" => $review->avatar,
                    "data" => array(
                        "thumbnail" => '/uploads/reviews/' . $review->avatar,
                    ),
                ];
            endif;

            $avatar = isset($avatar) ? json_encode($avatar) : null;

        }else{
            $avatar = NULL;
        }

        if($review->images){

            if(file_exists($dir . $review->images)):

                $images = [
                    "name" => $review->images,
                    "size" => filesize($dir . $review->images),
                    "file" => $review->images,
                    "data" => array(
                        "thumbnail" => '/uploads/reviews/' . $review->images,
                    ),
                ];
            endif;

            $images = isset($images) ? json_encode($images) : null;

        }else{
            $images = NULL;
        }

        $data = [
            'name' => 'Редактирование отзыва - '.$old['name'],
            'data' => $old,
            'project' => $review,
            'avatar' => $avatar,
            'images' => $images
        ];

        return view('backend.review.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Review $review)
    {
        $input = $request->except('_token');

        // dd($input['source']);

        $dir = public_path('uploads/reviews/');

        if(!json_decode($request->avatar, true)){

            File::delete($dir . $review->avatar);

            $input['avatar'] = null;
            
        }

        if($request->file('avatar')):
            $dir = public_path('uploads/reviews/');
            
            if($review->avatar):   
                File::delete($dir . $review->avatar);
            endif;
        
            $file = $request->file('avatar');
            $name = Str::random(5) .'.' . $file->getClientOriginalExtension() ?: 'jpg';
            $img = Image::make($file);
            $img->save($dir . $name);
            $input['avatar'] = $name;
        else:

            if(!json_decode($request->avatar, true)):
                File::delete($dir . $review->avatar);
    
                $input['avatar'] = null;
            else:
                $input['avatar'] = $review->avatar;
            endif;
            
        endif;

        if($request->file('images')):
            $dir = public_path('uploads/reviews/');
            
            if($review->images):   
                File::delete($dir . $review->avatar);
            endif;
        
            $file = $request->file('images');
            $name = Str::random(5) .'.' . $file->getClientOriginalExtension() ?: 'jpg';
            $img = Image::make($file);
            $img->save($dir . $name);
            $input['images'] = $name;
        else:

            if(!json_decode($request->images, true)):
                File::delete($dir . $review->images);
    
                $input['images'] = null;
            else:
                $input['images'] = $review->images;
            endif;

        endif;


        $review->fill($input);

        if($review->update()) {  
            return redirect()->route('reviews.index')->with('status', 'Информация обновлена');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review)
    {
        $dir = public_path('uploads/reviews/');
        File::delete($dir . $review->avatar);
        File::delete($dir . $review->images);
        $review->delete();

        return redirect()->route('reviews.index')->with('status','Отзыв удален');
    }
}
