<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Review;
use App\City;

class ApiController extends Controller
{
    public function projects(Request $request) {
        
        return [
            'projects' => Project::All()
        ];
    }

    public function reviews(Request $request) {
        
        return [
            'reviews' => Review::All()
        ];
    }
}
