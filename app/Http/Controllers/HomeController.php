<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($city)
    {
        $city = City::where('url', $city)->first();

        if($city){

            return view('welcome', [
                'city' => $city
            ]);

        }else{
            abort(404);
        }

        
    }
}
