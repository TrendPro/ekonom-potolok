<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;

class ImagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function image(Request $request)
    {
        $src = $request->image;
        $cachedImage = Image::cache(function($image) use ($src) {
            return $image->make('uploads/reviews/'.$src)->resize(100,100);
        }, 1, false);

        return Response::make($cachedImage, 200, array('Content-Type' => 'image/jpeg'));
    }
}
