<?php

namespace App\Http\Controllers;

use App\Lead;
use Illuminate\Http\Request;
use App\Connectors\BitrixConnector;

class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leads = Lead::All();

        $data = [
            'title' => 'Заявки',
            'leads' => $leads,
        ];

        return view('backend.lead.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');

        $lead = Lead::create($input);

        $bitrixConnector = new BitrixConnector;
        $bitrixConnector->addLead([
            'title' => $request->name ? 'Новый лид эконом потолок': $request->name,
            'name'  =>  $request->name,
            'phone' =>  '+7' . $request->phone,
            'direction' =>  56,
            'source'    =>  56,
            'city' => $request->bx_code
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function show(Lead $lead)
    {
        $data = [
            'title' => 'Заявка',
            'lead' => $lead
        ];

        return view('backend.lead.show',$data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lead $lead)
    {
        $lead->delete();
        return redirect()->route('leads.index')->with('status','Заявка удалена');
    }
}
