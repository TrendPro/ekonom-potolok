<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::All();

        $data = [
            'title' => 'Города',
            'cities' => $cities,
        ];

        return view('backend.city.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'Новый город',
        ];

        return view('backend.city.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');
         
        $city = City::create($input);

        if ($city->save()) {    
            return redirect()->route('cities.index')->with('status','Город добавлен');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {

        $old = $city->toArray();

        $data = [
            'name' => 'Редактирование города - '.$old['name'],
            'data' => $old,
            'city' => $city
        ];

        return view('backend.city.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        $input = $request->except('_token');

        $city->fill($input);

        if($city->update()) {  
            return redirect()->route('cities.index')->with('status', 'Информация обновлена');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        $city->delete();

        return redirect()->route('cities.index')->with('status','Город удален');
    }
}
