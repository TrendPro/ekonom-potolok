<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = [
        'name', 'avatar', 'text', 'images', 'date', 'status', 'source'
    ];

}