<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255);
            $table->string('name_formatted',255);
            $table->string('url',255);
            $table->string('bx_code',255);
            $table->string('email',255)->nullable();
            $table->string('phone',255)->nullable();
            $table->string('whatsapp',255)->nullable();
            $table->enum('status', ['active', 'deactived'])->default('active');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
