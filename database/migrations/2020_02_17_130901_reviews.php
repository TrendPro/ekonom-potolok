<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Reviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255)->nullable();
            $table->string('avatar',255)->nullable();
            $table->text('text')->nullable();
            $table->string('images',255)->nullable();
            $table->string('date',255)->nullable();
            $table->enum('status', ['active', 'deactived'])->default('active');
            $table->enum('source', ['inst', 'facebook', 'vk', 'ok'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
