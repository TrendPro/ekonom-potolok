const mix = require('laravel-mix');
const mqpacker = require("css-mqpacker");
const sortCSSmq = require('sort-css-media-queries');

/*
|--------------------------------------------------------------------------
| Mix Asset Management
|--------------------------------------------------------------------------
|
| Mix provides a clean, fluent API for defining some Webpack build steps
| for your Laravel application. By default, we are compiling the Sass
| file for the application as well as bundling up all the JS files.
|
*/

mix.js('resources/js/app_front.js', 'public/js')
//    .sass('resources/sass/app_front.scss', 'public/css')
//    .options({
//       postCss: [
//          mqpacker({
//                sort: sortCSSmq
//          })
//       ],
//       autoprefixer: {
//          options: {
//              browsers: [
//                  'last 6 versions',
//              ]
//          }
//      }
//    })
   .browserSync({ 
      proxy: "econom-potolok.local",
      open: false
  });;

// if (mix.inProduction()) {
//    mix.version();
// }
