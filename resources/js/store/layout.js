import axios from 'axios';


export default {
    state: {
        reviews: [],
        city: window.city,
        isSended: false,
        formModal: false,
        successSend: false,
        gift: false,
        nameSended: '',
        phoneSended: ''
    },
    mutations: {
        updateProjects (state, projects) {
            state.projects = projects
        },
        updateReviews (state, reviews) {
            state.reviews = reviews
        },
        isSendedAlert(state) {
            state.isSended = !state.isSended
        },
        openFormModal(state, tag) {
            if(Vue.cookie.get('form_send')){
                state.isSended = true
            }else{
                if(tag === 'gift'){
                    state.formModal = true
                    state.gift = true
                }
                else{
                    state.formModal = true
                }
            }
        },
        closeFormModal(state) {
            state.formModal = false
            state.gift = false
        },
        successSendForm(state) {
            state.successSend = true
        },
        updateNameSended (state, value) {
            state.nameSended = value
        },
        updatePhoneSended (state, value) {
            state.phoneSended = value
        }
    },
    actions: {
        async fetchReviews(context) {
            axios
            .get('/api/reviews/')
            .then(response => {
                let reviews = response.data.reviews
                context.commit('updateReviews', reviews)
            })
        }, 
        isSendedAlert(context) {
            context.commit('isSendedAlert')
        },
        openFormModal(context, tag) {
            context.commit('openFormModal', tag)
        },
        closeFormModal(context) {
            context.commit('closeFormModal')
        },
        successSendForm(context) {
            context.commit('successSendForm')
        }
    },
    getters: {
        reviews(state) {
            return state.reviews
        },
        city(state) {
            return state.city
        },
        isSended(state) {
            return state.isSended
        },
        formModal(state) {
            return state.formModal
        },
        successSend(state) {
            return state.successSend
        },
        gift(state) {
            return state.gift
        },
        nameSended(state) {
            return state.nameSended
        },
        phoneSended(state) {
            return state.phoneSended
        }
    }
}