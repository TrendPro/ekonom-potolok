import Vue from 'vue';
import store from './store';
window.Vue = Vue;

Vue.component('layout', require('./views/Layout.vue').default);

new Vue({
    el: '#app',
    store
})