<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="yandex-verification" content="72fd1a6b7706a65f" />
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Заказать натяжные потолки от производителя | Эконом Потолок</title>
        <meta name="description" content="Купите качественные натяжные потолки по доступной цене от производителя. Бесплатный замер. Гарантийное обслуживание." />

        <link rel="preload" href="fonts/jost-v1-latin_cyrillic-500.woff2" as="font" type="font/woff2" crossorigin>
        <link rel="preload" href="fonts/jost-v1-latin_cyrillic-700.woff2" as="font" type="font/woff2" crossorigin>
        <link rel="stylesheet" href="css/app_front.css">

        <style>
            /* jost-500 - latin_cyrillic */
            @font-face {
                font-family: 'Jost';
                font-display: swap;
                font-style: normal;
                font-weight: 500;
                src: url('fonts/jost-v1-latin_cyrillic-500.eot'); /* IE9 Compat Modes */
                src: local(''),
                    url('fonts/jost-v1-latin_cyrillic-500.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
                    url('fonts/jost-v1-latin_cyrillic-500.woff2') format('woff2'), /* Super Modern Browsers */
                    url('fonts/jost-v1-latin_cyrillic-500.woff') format('woff'), /* Modern Browsers */
                    url('fonts/jost-v1-latin_cyrillic-500.ttf') format('truetype'), /* Safari, Android, iOS */
                    url('fonts/jost-v1-latin_cyrillic-500.svg#Jost') format('svg'); /* Legacy iOS */
            }
            /* jost-regular - latin_cyrillic */
            @font-face {
                font-family: 'Jost';
                font-display: swap;
                font-style: normal;
                font-weight: 400;
                src: url('fonts/jost-v1-latin_cyrillic-regular.eot'); /* IE9 Compat Modes */
                src: local(''),
                    url('fonts/jost-v1-latin_cyrillic-regular.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
                    url('fonts/jost-v1-latin_cyrillic-regular.woff2') format('woff2'), /* Super Modern Browsers */
                    url('fonts/jost-v1-latin_cyrillic-regular.woff') format('woff'), /* Modern Browsers */
                    url('fonts/jost-v1-latin_cyrillic-regular.ttf') format('truetype'), /* Safari, Android, iOS */
                    url('fonts/jost-v1-latin_cyrillic-regular.svg#Jost') format('svg'); /* Legacy iOS */
            }
            /* jost-700 - latin_cyrillic */
            @font-face {
                font-family: 'Jost';
                font-display: swap;
                font-style: normal;
                font-weight: 700;
                src: url('fonts/jost-v1-latin_cyrillic-700.eot'); /* IE9 Compat Modes */
                src: local(''),
                    url('fonts/jost-v1-latin_cyrillic-700.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
                    url('fonts/jost-v1-latin_cyrillic-700.woff2') format('woff2'), /* Super Modern Browsers */
                    url('fonts/jost-v1-latin_cyrillic-700.woff') format('woff'), /* Modern Browsers */
                    url('fonts/jost-v1-latin_cyrillic-700.ttf') format('truetype'), /* Safari, Android, iOS */
                    url('fonts/jost-v1-latin_cyrillic-700.svg#Jost') format('svg'); /* Legacy iOS */
            }
        </style>

        <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
        <? if (!empty($city)): ?>
            <script type="text/javascript">
                window.city = <?= json_encode($city) ?>
            </script>
        <? endif ?>
    </head>
    <body>

        <div id="app">
            <layout /> 
        </div>

        <script src="js/app_front.js"></script>

        
        <!-- Yandex.Metrika counter --> 
        <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(48259676, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true }); </script> <noscript><div><img src="https://mc.yandex.ru/watch/48259676" style="position:absolute; left:-9999px;" alt="" /></div></noscript> 
        <!-- /Yandex.Metrika counter -->
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-85472546-9"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-85472546-9');
        </script>
        <!-- /Global site tag (gtag.js) - Google Analytics -->


        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '546799856233198');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=546799856233198&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
        @include('svg_sprite')
    </body>
</html>
