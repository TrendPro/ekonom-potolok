@extends('backend.index')

@section('section')

<div class="container-fluid">
    <h4 class="c-grey-900 mT-10 mB-30">{{$title}}</h4>

    <div class="row">
        <div class="col-md-12">
            <div class="bgc-white bd bdrs-3 p-20 mB-20">

                <div class="text-right form-group">
                    <a href="{{ route('reviews.create') }}" class="btn cur-p btn-primary">Создать</a>
                </div>

                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Имя</th>
                            <th scope="col">Аватар</th>
                            <th scope="col">Текст</th>
                            <th scope="col">Изображение</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    
                    <tbody>
                       @foreach($reviews as $review)
                            <tr {!! $review->status === 'deactived' ? 'style="background-color: rgba(0,0,0,.05);}"' : '' !!}>
                                <th scope="row">{{ $review->id }}</th>
                                <td>{{ $review->name }}</td>
                                <td>{{ $review->avatar}}</td>
                                <td>{{ $review->text}}</td>
                                <td>{{ $review->images}}</td>
                                <td>

                                    {!! Form::open(['url' => route('reviews.destroy',['review'=>$review->id]),'class'=>'text-right  gap-10','method'=>'POST']) !!}

                                    {{ method_field('DELETE') }}

                                    {!! Form::button('Удалить', ['class' => 'btn cur-p btn-outline-danger','type'=>'submit']) !!}

                                    {!! Html::link(route('reviews.edit',['review'=>$review->id]),'Редактировать',['class'=>'btn cur-p btn-outline-primary']) !!}

                                    {!! Form::close() !!}

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

@endsection
