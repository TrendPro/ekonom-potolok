@extends('backend.index') 
@section('section')

{!! Form::open(['url' => route('reviews.store'), 'class'=>'row', 'method'=>'POST','enctype'=>'multipart/form-data']) !!}

<div class="col-sm-12 form-row mT-10 mB-10">
    <h4 class="col-sm-6 c-grey-900">{{$title}}</h4>
    <div class="text-right col-sm-6">
       
        {!! Html::link(route('reviews.index'),'Вернуться к списку',['class'=>'btn cur-p btn-secondary']) !!}
        {{ Form::submit('Сохранить', array('class' => 'btn btn-primary')) }}
        
    </div>
</div>

<div class="col-sm-12 mT-20">
    <div class="bgc-white p-20 bd">
        <div class="mT-30">
            <div class="form-row">
                <div class="form-group col-md-6">
                    {{ Form::label('name', 'Имя') }} 
                    {{ Form::text('name', null, array('class'=>'form-control')) }}
                </div>
                <div class="form-group col-md-6">
                    {{ Form::label('status', 'Статус') }}
                    {!! Form::select('status', array('active' => 'Активно', 'deactived' => 'Скрыто'), null,  array('class' => 'form-control')) !!} 
                </div>
                <div class="form-group col-md-6">
                    {{ Form::label('source', 'Источник отзыва') }}
                    {!! Form::select('source', array('' => 'Не выбрано', 'inst' => 'instagram', 'facebook' => 'facebook', 'vk' => 'ВКонтакте', 'ok' => 'Одноклассники'), null,  array('class' => 'form-control')) !!} 
                </div>
                <div class="form-group col-md-6">
                    {{ Form::label('date', 'Дата') }} 
                    {{ Form::text('date', null, array('class'=>'form-control')) }} 
                </div>
            </div>
            <div class="form-row mT-30">
                <div class="form-group col-md-12">
                    {{ Form::label('text', 'Текст отзыва') }}
                    {{ Form::textarea('text', null, array('class' => 'form-control')) }}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12 mT-20">
    <div class="bgc-white p-20 bd">
        <div class="mT-30">
            <div class="form-row">
                <div class="col-md-12">
                    <div class="mT-30">
                        <div class="form-group">
                            {{ Form::label('avatar', 'Аватар') }}
                            {!! Form::file('avatar[]', array('class' => 'general-img')) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="mT-30">
                        <div class="form-group">
                            {{ Form::label('images', 'Изображение к отзыву') }}
                            {!! Form::file('images[]', array('class' => 'general-img')) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



{!! Form::close() !!}


@endsection
