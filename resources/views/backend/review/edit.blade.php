@extends('backend.index') 
@section('section')

{!! Form::open(['url' => route('reviews.update',array('review'=>$data['id'])), 'class'=>'row', 'method'=>'POST','enctype'=>'multipart/form-data']) !!} 
{!! Form::hidden('_method', 'patch') !!} 
{!! Form::hidden('id', $data['id']) !!}

<div class="col-sm-12 form-row mT-10 mB-10">
    <h4 class="col-sm-6 c-grey-900">{{$name}}</h4>
    <div class="text-right col-sm-6">
       
        {!! Html::link(route('reviews.index'),'Вернуться к списку',['class'=>'btn cur-p btn-secondary']) !!}
        {{ Form::submit('Сохранить', array('class' => 'btn btn-primary')) }}
        
    </div>
</div>

<div class="col-sm-12 mT-20">
    <div class="bgc-white p-20 bd">
        <div class="mT-30">
            <div class="form-row">
                <div class="form-group col-md-6">
                    {{ Form::label('name', 'Имя') }} 
                    {{ Form::text('name', $data['name'], array('class'=>'form-control')) }}
                </div>
                <div class="form-group col-md-6">
                    {{ Form::label('status', 'Статус') }}
                    {!! Form::select('status', array('active' => 'Активно', 'deactived' => 'Скрыто'), $data['status'],  array('class' => 'form-control')) !!} 
                </div>
                <div class="form-group col-md-6">
                    {{ Form::label('source', 'Источник отзыва') }}
                    {!! Form::select('source', array('' => 'Не выбрано', 'inst' => 'instagram', 'facebook' => 'facebook', 'vk' => 'ВКонтакте', 'ok' => 'Одноклассники'), $data['source'],  array('class' => 'form-control')) !!} 
                </div>
                <div class="form-group col-md-6">
                    {{ Form::label('date', 'Дата') }} 
                    {{ Form::text('date', $data['date'], array('class'=>'form-control')) }} 
                </div>
            </div>
            <div class="form-row mT-30">
                <div class="form-group col-md-12">
                    {{ Form::label('text', 'Текст отзыва') }}
                    {{ Form::textarea('text', $data['text'], array('class' => 'form-control')) }}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12 mT-20">
    <div class="bgc-white p-20 bd">
        <div class="mT-30">
            <div class="form-row">
                <div class="col-md-12">
                    <div class="mT-30">
                        <div class="form-group">
                            {{ Form::label('avatar', 'Аватар') }}
                            {!! Form::file('avatar', array('class' => 'general-img', 'data-fileuploader-listInput' => 'avatar', 'data-fileuploader-files' => $avatar)) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="mT-30">
                        <div class="form-group">
                            {{ Form::label('images', 'Изображение к отзыву') }}
                            {!! Form::file('images', array('class' => 'general-img', 'data-fileuploader-listInput' => 'images', 'data-fileuploader-files' => $images)) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{!! Form::close() !!}

@endsection