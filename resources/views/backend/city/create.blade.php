@extends('backend.index') 
@section('section')

{!! Form::open(['url' => route('cities.store'), 'class'=>'row', 'method'=>'POST','enctype'=>'multipart/form-data']) !!}

<div class="col-sm-12 form-row mT-10 mB-10">
    <h4 class="col-sm-6 c-grey-900">{{$title}}</h4>
    <div class="text-right col-sm-6">
       
        {!! Html::link(route('cities.index'),'Вернуться к списку',['class'=>'btn cur-p btn-secondary']) !!}
        {{ Form::submit('Сохранить', array('class' => 'btn btn-primary')) }}
        
    </div>
</div>

<div class="col-sm-12 mT-20">
    <div class="bgc-white p-20 bd">
        <div class="mT-30">
            <div class="form-row">
                <div class="form-group col-md-3">
                    {{ Form::label('name', 'Название') }} 
                    {{ Form::text('name', null, array('class'=>'form-control')) }}
                </div>
                <div class="form-group col-md-3">
                    {{ Form::label('name_formatted', 'Форматированное название') }} 
                    {{ Form::text('name_formatted', null, array('class'=>'form-control')) }}
                </div>
                <div class="form-group col-md-3">
                    {{ Form::label('url', 'URL') }} 
                    {{ Form::text('url', null, array('class'=>'form-control')) }}
                </div>
                <div class="form-group col-md-3">
                    {{ Form::label('bx_code', 'Код города в Битрикс') }} 
                    {{ Form::text('bx_code', null, array('class'=>'form-control')) }}
                </div>
                <div class="form-group col-md-3">
                    {{ Form::label('email', 'E-mail') }} 
                    {{ Form::text('email', null, array('class'=>'form-control')) }}
                </div>
                <div class="form-group col-md-3">
                    {{ Form::label('phone', 'Телефон') }} 
                    {{ Form::text('phone', null, array('class'=>'form-control')) }}
                </div>
                <div class="form-group col-md-3">
                    {{ Form::label('whatsapp', 'WhatsApp') }} 
                    {{ Form::text('whatsapp', null, array('class'=>'form-control')) }}
                </div>
                <div class="form-group col-md-3">
                    {{ Form::label('status', 'Статус') }}
                    {!! Form::select('status', array('active' => 'Активно', 'deactived' => 'Скрыто'), null,  array('class' => 'form-control')) !!} 
                </div>
            </div>
        </div>
    </div>
</div>

{!! Form::close() !!}


@endsection
