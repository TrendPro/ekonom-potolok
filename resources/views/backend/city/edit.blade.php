@extends('backend.index') 
@section('section')

{!! Form::open(['url' => route('cities.update',array('city'=>$data['id'])), 'class'=>'row', 'method'=>'POST','enctype'=>'multipart/form-data']) !!} 
{!! Form::hidden('_method', 'patch') !!} 
{!! Form::hidden('id', $data['id']) !!}

<div class="col-sm-12 form-row mT-10 mB-10">
    <h4 class="col-sm-6 c-grey-900">{{$name}}</h4>
    <div class="text-right col-sm-6">
       
        {!! Html::link(route('cities.index'),'Вернуться к списку',['class'=>'btn cur-p btn-secondary']) !!}
        {{ Form::submit('Сохранить', array('class' => 'btn btn-primary')) }}
        
        
    </div>
</div>

<div class="col-sm-12 mT-20">
    <div class="bgc-white p-20 bd">
        <div class="mT-30">
            <div class="form-row">
                <div class="form-group col-md-3">
                    {{ Form::label('name', 'Название') }} 
                    {{ Form::text('name', $data['name'], array('class'=>'form-control')) }}
                </div>
                <div class="form-group col-md-3">
                    {{ Form::label('name_formatted', 'Форматированное название') }} 
                    {{ Form::text('name_formatted', $data['name_formatted'], array('class'=>'form-control')) }}
                </div>
                <div class="form-group col-md-3">
                    {{ Form::label('url', 'URL') }} 
                    {{ Form::text('url', $data['url'], array('class'=>'form-control')) }}
                </div>
                <div class="form-group col-md-3">
                    {{ Form::label('bx_code', 'Код города в Битрикс') }} 
                    {{ Form::text('bx_code', $data['bx_code'], array('class'=>'form-control')) }}
                </div>
                <div class="form-group col-md-3">
                    {{ Form::label('email', 'E-mail') }} 
                    {{ Form::text('email', $data['email'], array('class'=>'form-control')) }}
                </div>
                <div class="form-group col-md-3">
                    {{ Form::label('phone', 'Телефон') }} 
                    {{ Form::text('phone', $data['phone'], array('class'=>'form-control')) }}
                </div>
                <div class="form-group col-md-3">
                    {{ Form::label('whatsapp', 'WhatsApp') }} 
                    {{ Form::text('whatsapp', $data['whatsapp'], array('class'=>'form-control')) }}
                </div>
                <div class="form-group col-md-3">
                    {{ Form::label('status', 'Статус') }}
                    {!! Form::select('status', array('active' => 'Активно', 'deactived' => 'Скрыто'), $data['status'],  array('class' => 'form-control')) !!} 
                </div>
            </div>
        </div>
    </div>
</div>


{!! Form::close() !!}

@endsection