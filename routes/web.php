<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::group(['prefix' => 'backend', 'middleware' => 'auth'], function () {

    Route::view('/', 'backend.index');
    Route::resource('projects', 'ProjectController' , ['except' => 'show']);

    Route::resource('reviews', 'ReviewController' , ['except' => 'show']);

    Route::group(['prefix'=>'leads'], function() {
        Route::get('/', 'LeadController@index')->name('leads.index');
        Route::get('{lead}', 'LeadController@show')->name('leads.show');
        Route::delete('{lead}', 'LeadController@destroy')->name('leads.destroy');
    });

    Route::resource('cities', 'CityController' , ['except' => 'show']);

});

Route::post('/leads', 'LeadController@store')->name('leads.store');


Route::get('/{city}', 'HomeController@index')->name('home');

Route::get('/{city}', 'HomeController@index')->name('home');

Route::get('/', function () {
    return view('welcome', [
        'city' => [
            'name' => 'Россия',
            'url' => 'россия',
            'bx_code' => '786'
        ]
    ]);
});

Route::get('/imager/{image}', 'ImagerController@image')->name('imager.image');

// Route::get('/', function () {
//     return redirect()->route('leads.index');
// });
